import traceback, sys, os
from app import app
from subprocess import Popen,PIPE
from flask_restful import Resource, url_for, request
from app.mod_apisv1.common.utils import return_error

class CaptureRes(Resource):
    def get (self):
        try:
            if not os.getenv('DISPLAY'):
                os.putenv("DISPLAY", '127.0.0.1:0')

            height = request.args.get('height')
            if not height: 
                return return_error(401, 'height parameter required')

            width = request.args.get('width')
            if not width: 
                return return_error(401, 'width parameter required')

            url = request.args.get('url')
            if not url: 
                return return_error(401, 'url parameter required')

            name = request.args.get('name')
            if not name: 
                return return_error(401, 'name parameter required')

            command = '''wkhtmltoimage --height %s --width %s --enable-javascript --javascript-delay 5000 %s %s''' % (height, width, url, name)
            p = Popen(command, shell=True,stdout=PIPE, stderr=PIPE, close_fds=True, cwd=app.config['STATIC_DIR'])
            stdout, stderr = p.communicate()

            if p.returncode != 0:
                return return_error(401, 'Gagal screenshoot')

            data_out = {
                'url': '%s%s' % (app.config['STATIC_URL'], name)
            }
            
            return data_out

        except Exception as e:
            print(e)
            ex_type, ex, tb = sys.exc_info()
            traceback.print_tb(tb)
            return return_error(500, 'API Error')